"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = {
    // capabilities: {
    //     'directConnect': true,
    //     'browserName': 'chrome',
    //     chromeOptions: {
    //         args: ["--headless", "--disable-gpu", "--window-size=800x600"]
    //     }},
    seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
    // seleniumAddress: 'http://localhost:8080/wd/hub',
    // capabilities: {
    //     'browserName': 'chrome'
    // },
    framework: 'jasmine',
    specs: ['./specs/**/*.js'],
    jasmineNodeOpts: {
        defaultTimeoutInterval: 90000
    },
    onPrepare: function () {
        var globals = require('protractor');
        var browser = globals.browser;
        browser.manage().window().maximize();
        browser.manage().timeouts().implicitlyWait(5000);
    }
};
